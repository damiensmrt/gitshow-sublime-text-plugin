import sublime, sublime_plugin
import subprocess
import os

#TODO - Extend the Git plug instead of this being a stand alone
class GitShowCommand(sublime_plugin.TextCommand):
	gitRootDir = ''
	currentFile = ''
	gitCurrentFile = ''

	def run(self, edit):
		self.edit = edit
		self.currentFile = self.view.file_name()
		os.chdir(os.path.dirname(self.currentFile))

		(out, error) = subprocess.Popen(["git", "rev-parse","--show-toplevel"],stdout=subprocess.PIPE).communicate()
		self.gitRootDir = out.rstrip().decode("utf-8")
		
		#TODO - Change this to a list view of remote branches
		self.view.window().show_input_panel("Branch", "master", self.on_done, None, None)
		self.gitCurrentFile = self.currentFile.replace(self.gitRootDir+'/','')

	def on_done(self,input):
		(out, error) = subprocess.Popen(["git", "show",input+":"+self.gitCurrentFile],stdout=subprocess.PIPE).communicate()
		self.view.window().run_command('temp_file_insert',{'output':out.decode("utf-8"),'name':self.view.name()})

#TODO - Need sytax highlighting
#TODO - Disable save to make it truely temp
class TempFileInsert(sublime_plugin.TextCommand):
	def run(self,edit,output,name):
		new_view = self.view.window().new_file()
		new_view.set_name(name)
		new_view.insert(edit,0,output)